﻿// Decompiled with JetBrains decompiler
// Type: ImporterDTCom.Service1
// Assembly: ImporterDTCom, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E3F90BC6-0982-4DED-B7A7-209FB441919F
// Assembly location: C:\Users\Urgimchak\Desktop\Import1C.exe

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;

namespace ImporterDTCom
{
  public class Service1 : ServiceBase
  {
    private const string sEventSource = "ImporterDTCom";
    private const string sEventLog = "";
    private BackgroundWorker Bw;
    private bool Work;
    private SqlConnection cn;
    private SqlConnection cn2;
    private SqlCommand cmd;
    private SqlParameter param;
    private IContainer components;

    public Service1()
    {
      this.Bw = new BackgroundWorker();
      this.InitializeComponent();
    }

    public void Importer(object sender, DoWorkEventArgs e)
    {
      int num1 = 20000;
      do
      {
        int num2 = 0;
        try
        {
          this.Maintimer_Tick();
        }
        catch (Exception ex)
        {
          this.WriteLog("\r\nImporter() " + ex.Message + "\t\r\n\r\n");
        }
        do
        {
          Thread.Sleep(500);
          num2 += 500;
        }
        while (num2 < num1);
      }
      while (this.Work);
    }

    public void Maintimer_Tick()
    {
      this.cn = new SqlConnection("Data Source=46.254.21.192;Connect Timeout=180;Initial Catalog=DT.com;user id=diplomtime_sql;password=333999YYTT55544$a13;Pooling=false;");
      this.cn2 = new SqlConnection("Server=192.168.0.8;Database=intexOrder;Password=vxcvk64-0gvd221S@;User ID=subLogin;Pooling=False;");
      DataTable dataTable1 = new DataTable();
      DataTable dataTable2 = this.import_SelectOrder();
      for (int index1 = 0; index1 < dataTable2.Rows.Count; ++index1)
      {
        int result1 = 0;
        try
        {
          int.TryParse(dataTable2.Rows[index1]["pagecnt"].ToString(), out result1);
        }
        catch
        {
        }
        double result2 = 0.0;
        try
        {
          double.TryParse(dataTable2.Rows[index1]["summa"].ToString(), out result2);
        }
        catch
        {
        }
        DateTime result3 = DateTime.Now;
        if (dataTable2.Rows[index1]["sroksdachi"] != DBNull.Value)
        {
          try
          {
            DateTime.TryParse(dataTable2.Rows[index1]["sroksdachi"].ToString(), out result3);
          }
          catch
          {
          }
        }
        DateTime result4 = DateTime.Now;
        if (dataTable2.Rows[index1]["datapriema"] != DBNull.Value)
        {
          try
          {
            DateTime.TryParse(dataTable2.Rows[index1]["datapriema"].ToString(), out result4);
          }
          catch
          {
          }
        }
        string psw = dataTable2.Rows[index1]["psw"].ToString().Length != 0 ? dataTable2.Rows[index1]["psw"].ToString() : new Random().Next(10000, 99999).ToString().Replace("1", "Y").Replace("21", "W").Replace("6", "A").Replace("34", "V").Replace("62", "Z");
        bool result5 = false;
        if (dataTable2.Rows[index1]["isorder"] == DBNull.Value || !bool.TryParse(dataTable2.Rows[index1]["isorder"].ToString(), out result5))
          result5 = false;
        int idadmin = this.import_NextAdmin(dataTable2.Rows[index1]["email"].ToString());
        string otkudab = "";
        string otkudapp = "";
        string otkudames = "";
        string str = dataTable2.Rows[index1]["OtherKnow"].ToString();
        string otkuda;
        if (str.Split('.').Length == 4)
        {
          otkuda = str.Split('.')[0].ToString();
          if (otkuda == "0")
            otkuda = "";
          otkudab = str.Split('.')[1].ToString();
          if (otkudab == "0")
            otkudab = "";
          otkudapp = str.Split('.')[2].ToString();
          if (otkudapp == "0")
            otkudapp = "";
          otkudames = str.Split('.')[3].ToString();
          if (otkudames == "0")
            otkudames = "";
        }
        else
          otkuda = str;
        int result6 = 0;
        if (!int.TryParse(dataTable2.Rows[index1]["posrednik"].ToString(), out result6))
          result6 = 6;
        int orderid = this.import_InsertOrder(Convert.ToInt32(dataTable2.Rows[index1]["id"]), dataTable2.Rows[index1]["klfam"].ToString(), dataTable2.Rows[index1]["klname"].ToString(), "", dataTable2.Rows[index1]["email"].ToString(), this.FormatPhone(dataTable2.Rows[index1]["phone"].ToString()), dataTable2.Rows[index1]["phone"].ToString(), "", "", otkuda, otkudab, otkudapp, otkudames, dataTable2.Rows[index1]["predmet"].ToString(), dataTable2.Rows[index1]["type"].ToString(), dataTable2.Rows[index1]["city"].ToString(), dataTable2.Rows[index1]["vuz"].ToString(), dataTable2.Rows[index1]["name"].ToString(), result1, result2, result3, result4, dataTable2.Rows[index1]["comment"].ToString(), psw, idadmin, 0, result5, result6);
        if (orderid > 0)
        {
          DataTable dataTable3 = new DataTable();
          DataTable dataTable4 = this.import_SelectFile(Convert.ToInt32(dataTable2.Rows[index1]["id"]));
          bool flag = true;
          for (int index2 = 0; index2 < dataTable4.Rows.Count; ++index2)
          {
            if (!this.import_InsertClFile(orderid, dataTable4.Rows[index2]["file"], dataTable4.Rows[index2]["filename"].ToString()))
            {
              this.WriteLog("\r\nERROR import_InsertClFile(" + dataTable4.Rows[index2]["filename"].ToString() + ") \t\r\n\r\n");
              flag = false;
              this.import_DeleteOrder(orderid);
              this.WriteLog("\r\nERROR import_DeleteOrder(" + orderid.ToString() + ") \t\r\n\r\n");
              break;
            }
          }
          if (flag)
            this.import_UpdateOrder(Convert.ToInt32(dataTable2.Rows[index1]["id"]));
        }
      }
      DataTable dataTable5 = new DataTable();
      DataTable dataTable6 = this.import_SelectAvtor();
      Random random = new Random();
      for (int index = 0; index < dataTable6.Rows.Count; ++index)
      {
        string psw = random.Next(1000, 99999).ToString().Replace("1", "a").Replace("2", "f").Replace("4", "z").Replace("8", "p");
        int result = 0;
        if (this.import_InsertAvtor(psw, dataTable6.Rows[index]["fio"].ToString(), dataTable6.Rows[index]["email"].ToString(), dataTable6.Rows[index]["city"].ToString(), dataTable6.Rows[index]["phone"].ToString(), dataTable6.Rows[index]["comment"].ToString()) > 0 && int.TryParse(dataTable6.Rows[index]["id"].ToString(), out result))
          this.import_UpdateAvtor(result);
      }
      if (this.cn != null)
      {
        this.cn.Close();
        this.cn.Dispose();
      }
      if (this.cn2 == null)
        return;
      this.cn2.Close();
      this.cn2.Dispose();
    }

    public DataTable import_SelectAvtor()
    {
      SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
      DataTable dataTable = new DataTable();
      try
      {
        this.cmd = new SqlCommand(nameof (import_SelectAvtor), this.cn);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.cmd.Connection.Open();
        sqlDataAdapter.SelectCommand = this.cmd;
        sqlDataAdapter.Fill(dataTable);
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_SelectOrder() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return dataTable;
    }

    public DataTable import_SelectOrder()
    {
      SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
      DataTable dataTable = new DataTable();
      try
      {
        this.cmd = new SqlCommand(nameof (import_SelectOrder), this.cn);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.cmd.Connection.Open();
        sqlDataAdapter.SelectCommand = this.cmd;
        sqlDataAdapter.Fill(dataTable);
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_SelectOrder() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return dataTable;
    }

    public DataTable import_SelectFile(int orderid)
    {
      SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
      DataTable dataTable = new DataTable();
      try
      {
        this.cmd = new SqlCommand(nameof (import_SelectFile), this.cn);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@id", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) orderid;
        this.cmd.Connection.Open();
        sqlDataAdapter.SelectCommand = this.cmd;
        sqlDataAdapter.Fill(dataTable);
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_SelectFile() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return dataTable;
    }

    public void import_UpdateAvtor(int id)
    {
      try
      {
        this.cmd = new SqlCommand(nameof (import_UpdateAvtor), this.cn);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@id", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) id;
        this.cmd.Connection.Open();
        this.cmd.ExecuteNonQuery();
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_UpdateOrder() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
    }

    public void import_UpdateOrder(int orderid)
    {
      try
      {
        this.cmd = new SqlCommand(nameof (import_UpdateOrder), this.cn);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@id", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) orderid;
        this.cmd.Connection.Open();
        this.cmd.ExecuteNonQuery();
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_UpdateOrder() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
    }

    public void import_DeleteOrder(int orderid)
    {
      try
      {
        this.cmd = new SqlCommand(nameof (import_DeleteOrder), this.cn2);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@id", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) orderid;
        this.cmd.Connection.Open();
        this.cmd.ExecuteNonQuery();
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_DeleteOrder() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
    }

    public int import_InsertAvtor(string psw, string name, string email, string city, string phone, string comment)
    {
      int num = 0;
      try
      {
        this.cmd = new SqlCommand(nameof (import_InsertAvtor), this.cn2);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@psw", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) psw;
        this.param = this.cmd.Parameters.Add("@phone", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) phone;
        this.param = this.cmd.Parameters.Add("@comment", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) comment;
        this.param = this.cmd.Parameters.Add("@name", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) name;
        this.param = this.cmd.Parameters.Add("@email", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) email;
        this.param = this.cmd.Parameters.Add("@city", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) city;
        this.cmd.Connection.Open();
        num = Convert.ToInt32(this.cmd.ExecuteScalar());
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_InsertAvtor() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return num;
    }

    public int import_InsertOrder(int orderid, string klfam, string klname, string klot, string email, string phone, string phonedef, string fac, string kur, string otkuda, string otkudab, string otkudapp, string otkudames, string science, string type, string city, string vuz, string name, int pagecnt, double summa, DateTime sroksdachi, DateTime datapriema, string comment, string psw, int idadmin, int partnerkod, bool isorder, int posrednik)
    {
      int num = 0;
      try
      {
        this.cmd = new SqlCommand(nameof (import_InsertOrder), this.cn2);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@orderid", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) orderid;
        this.param = this.cmd.Parameters.Add("@klfam", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) klfam;
        this.param = this.cmd.Parameters.Add("@klname", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) klname;
        this.param = this.cmd.Parameters.Add("@klot", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) klot;
        this.param = this.cmd.Parameters.Add("@email", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) email;
        this.param = this.cmd.Parameters.Add("@phone", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) phone;
        this.param = this.cmd.Parameters.Add("@phonedef", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) phonedef;
        this.param = this.cmd.Parameters.Add("@fac", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) fac;
        this.param = this.cmd.Parameters.Add("@kur", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) kur;
        this.param = this.cmd.Parameters.Add("@otkuda", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) otkuda;
        this.param = this.cmd.Parameters.Add("@otkudab", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) otkudab;
        this.param = this.cmd.Parameters.Add("@otkudapp", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) otkudapp;
        this.param = this.cmd.Parameters.Add("@otkudames", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) otkudames;
        this.param = this.cmd.Parameters.Add("@science", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) science;
        this.param = this.cmd.Parameters.Add("@type", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) type;
        this.param = this.cmd.Parameters.Add("@city", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) city;
        this.param = this.cmd.Parameters.Add("@vuz", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) vuz;
        this.param = this.cmd.Parameters.Add("@name", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) name;
        this.param = this.cmd.Parameters.Add("@pagecnt", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) pagecnt;
        this.param = this.cmd.Parameters.Add("@summa", SqlDbType.Real);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) summa;
        this.param = this.cmd.Parameters.Add("@sroksdachi", SqlDbType.DateTime);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) sroksdachi;
        this.param = this.cmd.Parameters.Add("@datapriema", SqlDbType.DateTime);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) datapriema;
        this.param = this.cmd.Parameters.Add("@comment", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) comment;
        this.param = this.cmd.Parameters.Add("@psw", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) psw;
        this.param = this.cmd.Parameters.Add("@AdminPrinimator", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) idadmin;
        this.param = this.cmd.Parameters.Add("@partnerkod", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) partnerkod;
        this.param = this.cmd.Parameters.Add("@isorder", SqlDbType.Bit);
        this.param.Direction = ParameterDirection.Input;
        if (isorder)
          this.param.Value = (object) 1;
        else
          this.param.Value = (object) 0;
        this.param = this.cmd.Parameters.Add("@posrednik", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) posrednik;
        this.cmd.Connection.Open();
        num = Convert.ToInt32(this.cmd.ExecuteScalar());
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_InsertOrder() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return num;
    }

    public bool import_InsertClFile(int orderid, object file, string filename)
    {
      bool flag = false;
      try
      {
        this.cmd = new SqlCommand(nameof (import_InsertClFile), this.cn2);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@orderid", SqlDbType.Int);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) orderid;
        this.param = this.cmd.Parameters.Add("@file", SqlDbType.Image);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = file;
        this.param = this.cmd.Parameters.Add("@filename", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) filename;
        this.cmd.Connection.Open();
        this.cmd.ExecuteNonQuery();
        flag = true;
      }
      catch (SqlException ex)
      {
        this.WriteLog("\r\nimport_InsertClFile() " + ex.Message + "\t\r\n\r\n");
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return flag;
    }

    public void WriteLog(string msg)
    {
      if (!EventLog.SourceExists("ImporterDTCom"))
        EventLog.CreateEventSource("ImporterDTCom", "");
      EventLog.WriteEntry("ImporterDTCom", msg, EventLogEntryType.Error);
    }

    public int import_NextAdmin(string email)
    {
      int num = 0;
      try
      {
        this.cmd = new SqlCommand("import_NextAdminWithOld", this.cn2);
        this.cmd.CommandType = CommandType.StoredProcedure;
        this.cmd.CommandTimeout = 360;
        this.param = this.cmd.Parameters.Add("@email", SqlDbType.Text);
        this.param.Direction = ParameterDirection.Input;
        this.param.Value = (object) email;
        this.cmd.Connection.Open();
        num = Convert.ToInt32(this.cmd.ExecuteScalar());
      }
      catch (SqlException ex)
      {
        this.WriteLog("method import_NextAdmin error - " + ex.Message);
      }
      finally
      {
        this.cmd.Connection.Close();
        this.cmd.Dispose();
      }
      return num;
    }

    protected override void OnStart(string[] args)
    {
      this.Work = true;
      this.Bw.DoWork += new DoWorkEventHandler(this.Importer);
      this.Bw.RunWorkerAsync();
    }

    protected override void OnStop()
    {
      this.Work = false;
      Thread.Sleep(500);
      Thread.Sleep(500);
      Thread.Sleep(500);
    }

    private string FormatPhone(string number)
    {
      string str1 = new Regex("\\D").Replace(number, "");
      switch (str1.Length)
      {
        case 6:
          string str2 = str1.Insert(str1.Length - 2, "-");
          str1 = str2.Insert(str2.Length - 5, "-");
          break;
        case 7:
          string str3 = str1.Insert(str1.Length - 2, "-");
          str1 = str3.Insert(str3.Length - 5, "-");
          break;
        case 10:
          if ((int) str1[0] == 57)
          {
            string str4 = str1.Insert(str1.Length - 2, "-");
            string str5 = str4.Insert(str4.Length - 5, "-");
            str1 = str5.Insert(str5.Length - 9, ") ").Insert(0, "(").Insert(0, "+7 ");
            break;
          }
          string str6 = str1.Insert(str1.Length - 2, "-");
          string str7 = str6.Insert(str6.Length - 5, "-");
          str1 = str7.Insert(str7.Length - 9, ") ").Insert(0, "(");
          break;
        case 11:
          string str8 = str1.Insert(str1.Length - 2, "-");
          string str9 = str8.Insert(str8.Length - 5, "-");
          string str10 = str9.Insert(str9.Length - 9, ") ").Insert(1, " (");
          str1 = (int) str10[0] != 55 ? ((int) str10[0] != 56 ? str10.Insert(0, "+") : "+7" + str10.Substring(1)) : str10.Insert(0, "+");
          break;
        case 12:
          if ((int) str1[0] == 48)
          {
            string str4 = "+" + str1.Substring(1);
            string str5 = str4.Insert(str4.Length - 2, "-");
            string str11 = str5.Insert(str5.Length - 5, "-");
            str1 = str11.Insert(str11.Length - 9, ") ").Insert(2, " (");
            break;
          }
          if ((int) str1[0] == 51 && (int) str1[1] == 56 && (int) str1[2] == 48)
          {
            string str4 = str1.Insert(str1.Length - 2, "-");
            string str5 = str4.Insert(str4.Length - 5, "-");
            str1 = str5.Insert(str5.Length - 9, ") ").Insert(3, " (").Insert(0, "+");
            break;
          }
          if ((int) str1[0] == 51 || (int) str1[0] == 55 || (int) str1[0] == 56)
          {
            string str4 = str1.Insert(str1.Length - 2, "-");
            string str5 = str4.Insert(str4.Length - 5, "-");
            str1 = str5.Insert(str5.Length - 9, ") ").Insert(1, " (").Insert(0, " +");
            break;
          }
          string str12 = str1.Insert(str1.Length - 2, "-");
          string str13 = str12.Insert(str12.Length - 5, "-");
          str1 = str13.Insert(str13.Length - 9, ") ").Insert(0, " (");
          break;
        default:
          if (str1.Length > 12)
          {
            if ((int) str1[0] == 48)
            {
              string str4 = "+" + str1.Substring(1);
              string str5 = str4.Insert(str4.Length - 2, "-");
              string str11 = str5.Insert(str5.Length - 5, "-");
              str1 = str11.Insert(str11.Length - 9, ") ").Insert(2, " (");
            }
            else if ((int) str1[0] == 51 && (int) str1[0] == 56 && (int) str1[0] == 48)
            {
              string str4 = str1.Insert(str1.Length - 2, "-");
              string str5 = str4.Insert(str4.Length - 5, "-");
              str1 = str5.Insert(str5.Length - 9, ") ").Insert(3, " (").Insert(0, "+");
            }
            else if ((int) str1[0] == 51 || (int) str1[0] == 55 || (int) str1[0] == 56)
            {
              string str4 = str1.Insert(str1.Length - 2, "-");
              string str5 = str4.Insert(str4.Length - 5, "-");
              str1 = str5.Insert(str5.Length - 9, ") ").Insert(1, " (").Insert(0, " +");
            }
            else if ((int) str1[0] == 57 && (int) str1[1] == 57 && (int) str1[2] == 54)
            {
              string str4 = str1.Insert(str1.Length - 2, "-");
              string str5 = str4.Insert(str4.Length - 5, "-");
              str1 = str5.Insert(str5.Length - 9, ") ").Insert(3, " (").Insert(0, "+");
            }
            else
            {
              string str4 = str1.Insert(str1.Length - 2, "-");
              string str5 = str4.Insert(str4.Length - 5, "-");
              str1 = str5.Insert(str5.Length - 9, ") ").Insert(0, " (");
            }
          }
          if (str1.Length > 7 && str1.Length < 10)
          {
            string str4 = str1.Insert(str1.Length - 2, "-");
            string str5 = str4.Insert(str4.Length - 5, "-");
            str1 = str5.Insert(str5.Length - 9, ") ").Insert(0, "(");
            break;
          }
          break;
      }
      return str1;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.ServiceName = nameof (Service1);
    }
  }
}
