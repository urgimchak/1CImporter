﻿// Decompiled with JetBrains decompiler
// Type: WindowsService1.ProjectInstaller
// Assembly: ImporterDTCom, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E3F90BC6-0982-4DED-B7A7-209FB441919F
// Assembly location: C:\Users\Urgimchak\Desktop\Import1C.exe

using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace WindowsService1
{
  [RunInstaller(true)]
  public class ProjectInstaller : Installer
  {
    private IContainer components;
    private ServiceProcessInstaller serviceProcessInstaller1;
    private ServiceInstaller ImporterComInst360023;

    public ProjectInstaller()
    {
      this.InitializeComponent();
    }

    private void ImporterInst2011_AfterInstall(object sender, InstallEventArgs e)
    {
    }

    private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
    {
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.serviceProcessInstaller1 = new ServiceProcessInstaller();
      this.ImporterComInst360023 = new ServiceInstaller();
      this.serviceProcessInstaller1.Account = ServiceAccount.LocalSystem;
      this.serviceProcessInstaller1.Password = (string) null;
      this.serviceProcessInstaller1.Username = (string) null;
      this.serviceProcessInstaller1.AfterInstall += new InstallEventHandler(this.serviceProcessInstaller1_AfterInstall);
      this.ImporterComInst360023.ServiceName = "ImporterCom360023";
      this.ImporterComInst360023.StartType = ServiceStartMode.Automatic;
      this.ImporterComInst360023.AfterInstall += new InstallEventHandler(this.ImporterInst2011_AfterInstall);
      this.Installers.AddRange(new Installer[2]
      {
        (Installer) this.serviceProcessInstaller1,
        (Installer) this.ImporterComInst360023
      });
    }
  }
}
