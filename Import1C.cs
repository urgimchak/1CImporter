﻿// Decompiled with JetBrains decompiler
// Type: ImporterDTCom.Import1C
// Assembly: ImporterDTCom, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E3F90BC6-0982-4DED-B7A7-209FB441919F
// Assembly location: C:\Users\Urgimchak\Desktop\Import1C.exe

using agsXMPP;
using agsXMPP.protocol.client;
using agsXMPP.Xml.Dom;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using agsXMPP.protocol.iq.@private;

namespace ImporterDTCom
{
  public class Import1C : ServiceBase
  {
    private Import1C.Payment NewPayment = new Import1C.Payment();
    private Import1C.Payment ModPayment = new Import1C.Payment();
    private string dir = "\\\\Db\\1C";
    private string host = "db";
    private string dbase = "IntexOrder";
    private string dtable = "";
    private string duser = "import1c";
    private string dpass = "import1c";
    private string JID_SENDER = "import1c@jabber.ru";
    private string PASSWORD = "Cfdrbyf2!";
    private SqlConnection conn = new SqlConnection();
    private string KodPosrednika = "";
    private IContainer components;
    private StreamWriter file;
    private System.Timers.Timer timer1;
    private XmppClientConnection xmpp;
    private int connect;
    private DirectoryInfo dirinfo;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.ServiceName = nameof (Import1C);
    }

    public Import1C()
    {
      this.InitializeComponent();
    }



    protected void OnStart(object sender)
    {
      ObjectHandler objectHandler1 = (ObjectHandler) null;
      ObjectHandler objectHandler2 = (ObjectHandler) null;
      this.file = new StreamWriter((Stream) new FileStream("\\\\db\\1c\\log\\Import1C.log", FileMode.Append));
      this.WriteToLog("Import1C стартовал");
      this.dirinfo = new DirectoryInfo(this.dir);
      this.conn.ConnectionString = "Data Source=" + this.host + ";Initial Catalog=" + this.dbase + ";User Id=" + this.duser + ";Password=" + this.dpass + ";";
      if (this.dirinfo.Exists)
      {
        Jid jid = new Jid(this.JID_SENDER);
        this.xmpp = new XmppClientConnection(jid.Server);
        this.xmpp.Open(jid.User, this.PASSWORD);
        XmppClientConnection xmpp1 = this.xmpp;
        if (objectHandler1 == null)
        {
          // ISSUE: method pointer
          //objectHandler1 = new ObjectHandler((object) this, __methodptr(\u003COnStart\u003Eb__0));
                    objectHandler1 = new ObjectHandler(OnStart);
        }
        ObjectHandler objectHandler3 = objectHandler1;
        xmpp1.OnLogin+=objectHandler3;
        XmppClientConnection xmpp2 = this.xmpp;
        if (objectHandler2 == null)
        {
          // ISSUE: method pointer
          objectHandler2 = new ObjectHandler(OnStart);
        }
        ObjectHandler objectHandler4 = objectHandler2;
        xmpp2.OnClose+=objectHandler4;
        this.timer1 = new System.Timers.Timer();
        this.timer1.Enabled = true;
        this.timer1.Interval = 10000.0;
        this.timer1.Elapsed += new ElapsedEventHandler(this.timer1_Elapsed);
        this.timer1.AutoReset = true;
        this.timer1.Start();
        this.WriteToLog("Директория обмена обнаружена, обработка запущена!");
      }
      else
        this.WriteToLog("Ошибка! Директории обмена не существует!");
    }

 

        protected override void OnStop()
    {
      if (this.connect == 1)
        ((XmppConnection) this.xmpp).Close();
      this.timer1.Stop();
      this.WriteToLog("Import1C остановлен");
      this.file.Close();
    }

    private void timer1_Elapsed(object sender, ElapsedEventArgs e)
    {
      if (this.dirinfo.Exists)
      {
        if (this.dirinfo.GetDirectories("arc").Length == 0)
          this.dirinfo.CreateSubdirectory("arc");
        if (this.dirinfo.GetDirectories("bad").Length == 0)
          this.dirinfo.CreateSubdirectory("bad");
        if (this.connect == 0)
          this.xmpp.Open(new Jid(this.JID_SENDER).User, this.PASSWORD);
        if (this.dirinfo.GetFiles("New*.csv").Length != 0)
        {
          foreach (FileInfo file in this.dirinfo.GetFiles("New*.csv"))
          {
            if (this.GetDataFromFile(file.DirectoryName, file.Name, 0) && this.SetDataInTable(file.DirectoryName, file.Name))
            {
              System.IO.File.Move(file.FullName, file.DirectoryName + "\\arc\\" + file.Name);
              this.WriteToLog("Файл " + file.Name + " успешно обработан! Перенесен в Arc!");
            }
          }
        }
        if (this.dirinfo.GetFiles("Mod*.csv").Length != 0)
        {
          foreach (FileInfo file in this.dirinfo.GetFiles("Mod*.csv"))
          {
            if (this.GetDataFromFile(file.DirectoryName, file.Name, 1) && this.ModDataInTable(file.DirectoryName, file.Name))
            {
              System.IO.File.Move(file.FullName, file.DirectoryName + "\\arc\\" + file.Name);
              this.WriteToLog("Файл " + file.Name + " успешно обработан! Перенесен в Arc!");
            }
          }
        }
        if (this.dirinfo.GetFiles("Del*.csv").Length == 0)
          return;
        foreach (FileInfo file in this.dirinfo.GetFiles("Del*.csv"))
        {
          if (this.GetDataFromFile(file.DirectoryName, file.Name, 0) && this.DelDataInTable(file.DirectoryName, file.Name))
          {
            System.IO.File.Move(file.FullName, file.DirectoryName + "\\arc\\" + file.Name);
            this.WriteToLog("Файл " + file.Name + " успешно обработан! Перенесен в Arc!");
          }
        }
      }
      else
        this.WriteToLog("Ошибка! Директории обмена не существует!");
    }

    private bool GetDataFromFile(string path, string filename, int readmode)
    {
      string str1 = path + "\\" + filename;
      StreamReader streamReader = new StreamReader(str1);
      string str2;
      if ((str2 = streamReader.ReadLine()) == null)
      {
        streamReader.Close();
        System.IO.File.Move(str1, path + "\\bad\\" + filename);
        this.WriteToLog("Файл " + filename + " пуст! Перенесен в BAD!");
        return false;
      }
      this.NewPayment.reset();
      string[] strArray1 = str2.Split(';');
      if (strArray1.Length != 5 || strArray1[0] == "" || (strArray1[1] == "" || strArray1[2] == "") || (strArray1[3] == "" || strArray1[4] == ""))
      {
        streamReader.Close();
        System.IO.File.Move(str1, path + "\\bad\\" + filename);
        this.WriteToLog("Файл " + filename + " имеет некорректную структуру! Перенесен в BAD!");
        return false;
      }
      this.NewPayment.Order = strArray1[0];
      this.NewPayment.Sum = strArray1[1];
      this.NewPayment.DateOplati = strArray1[2];
      this.NewPayment.Kassir = strArray1[3];
      this.NewPayment.Check = strArray1[4];
      if (readmode != 1)
      {
        streamReader.Close();
        return true;
      }
      string str3;
      if ((str3 = streamReader.ReadLine()) == null)
      {
        streamReader.Close();
        System.IO.File.Move(str1, path + "\\bad\\" + filename);
        this.WriteToLog("Файл " + filename + " имеет некорректную структуру! Перенесен в BAD!");
        return false;
      }
      this.ModPayment.reset();
      string[] strArray2 = str3.Split(';');
      if (strArray2.Length != 5 || strArray2[0] == "" || (strArray2[1] == "" || strArray2[2] == "") || (strArray2[3] == "" || strArray2[4] == ""))
      {
        streamReader.Close();
        System.IO.File.Move(str1, path + "\\bad\\" + filename);
        this.WriteToLog("Файл " + filename + " имеет некорректную структуру! Перенесен в BAD!");
        return false;
      }
      this.ModPayment.Order = strArray2[0];
      this.ModPayment.Sum = strArray2[1];
      this.ModPayment.DateOplati = strArray2[2];
      this.ModPayment.Kassir = strArray2[3];
      this.ModPayment.Check = strArray2[4];
      streamReader.Close();
      return true;
    }

    private bool SetDataInTable(string path, string filename)
    {
      bool flag;
      try
      {
        this.conn.Open();
        string sourceFileName = path + "\\" + filename;
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        if (this.GetSpr(ref this.NewPayment))
        {
          if (this.ChekOrdSum(this.NewPayment.Order, this.NewPayment.Sum))
          {
            sqlCommand.CommandText = "SELECT TOP 1 [id] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
            object obj1 = sqlCommand.ExecuteScalar();
            if (obj1 == null || obj1 == DBNull.Value)
            {
              sqlCommand.CommandText = "SELECT TOP 1 [Key] FROM [intexOrder].[dbo].[Заказы] WHERE [Key] =" + this.NewPayment.Order + " AND [MicroStad]>700  AND [MicroStad]<3200 ";
              object obj2 = sqlCommand.ExecuteScalar();
              if (obj2 == null || obj2 == DBNull.Value)
              {
                this.SendingNetSend(4);
                this.conn.Close();
                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                this.WriteToLog("В файле " + filename + " заказ не на стадии 1.3.*! Перенесен в BAD!");
                return false;
              }
              if (!this.FirstPayment(this.NewPayment.Order))
              {
                this.SendingNetSend(4);
                this.conn.Close();
                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                this.WriteToLog("Не удалось перевести заказ на стадию 2.0!(Файл " + filename + ")! Перенесен в BAD!");
                return false;
              }
              sqlCommand.CommandText = "INSERT INTO [intexOrder].[dbo].[PostupleniaFinansov] ([ZakazId],[Summ_1],[DateOplati_1],[Kassir_1],[DateTransaction_1],[Check_1]) VALUES (" + this.NewPayment.Order + "," + this.NewPayment.Sum + ",'" + this.NewPayment.DateOplati + "'," + this.NewPayment.Kassir + ",'" + this.NewPayment.DateTransaction + "'," + this.NewPayment.Check + ")";
              if (sqlCommand.ExecuteNonQuery() <= 0)
              {
                this.SendingNetSend(4);
                this.conn.Close();
                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                return false;
              }
              if (!this.MovToAllIn(this.NewPayment.Order))
              {
                this.SendingNetSend(4);
                this.conn.Close();
                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                return false;
              }
              this.SendingNetSend(1);
            }
            else
            {
              sqlCommand.CommandText = "SELECT TOP 1 [Summ_1] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
              if (sqlCommand.ExecuteScalar().ToString() == "0")
              {
                sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_1]=" + this.NewPayment.Sum + ",[DateOplati_1]='" + this.NewPayment.DateOplati + "',[Kassir_1]=" + this.NewPayment.Kassir + ",[DateTransaction_1]='" + this.NewPayment.DateTransaction + "',[Check_1]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                if (sqlCommand.ExecuteNonQuery() <= 0)
                {
                  this.SendingNetSend(4);
                  this.conn.Close();
                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                  this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                  return false;
                }
                if (!this.MovToAllIn(this.NewPayment.Order))
                {
                  this.SendingNetSend(4);
                  this.conn.Close();
                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                  this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                  return false;
                }
              }
              else
              {
                sqlCommand.CommandText = "SELECT TOP 1 [Summ_2] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                if (sqlCommand.ExecuteScalar().ToString() == "0")
                {
                  sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_2]=" + this.NewPayment.Sum + ",[DateOplati_2]='" + this.NewPayment.DateOplati + "',[Kassir_2]=" + this.NewPayment.Kassir + ",[DateTransaction_2]='" + this.NewPayment.DateTransaction + "',[Check_2]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                  if (sqlCommand.ExecuteNonQuery() <= 0)
                  {
                    this.SendingNetSend(4);
                    this.conn.Close();
                    System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                    this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                    return false;
                  }
                  if (!this.MovToAllIn(this.NewPayment.Order))
                  {
                    this.SendingNetSend(4);
                    this.conn.Close();
                    System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                    this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                    return false;
                  }
                }
                else
                {
                  sqlCommand.CommandText = "SELECT TOP 1 [Summ_3] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                  if (sqlCommand.ExecuteScalar().ToString() == "0")
                  {
                    sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_3]=" + this.NewPayment.Sum + ",[DateOplati_3]='" + this.NewPayment.DateOplati + "',[Kassir_3]=" + this.NewPayment.Kassir + ",[DateTransaction_3]='" + this.NewPayment.DateTransaction + "',[Check_3]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                    if (sqlCommand.ExecuteNonQuery() <= 0)
                    {
                      this.SendingNetSend(4);
                      this.conn.Close();
                      System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                      this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                      return false;
                    }
                    if (!this.MovToAllIn(this.NewPayment.Order))
                    {
                      this.SendingNetSend(4);
                      this.conn.Close();
                      System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                      this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                      return false;
                    }
                  }
                  else
                  {
                    sqlCommand.CommandText = "SELECT TOP 1 [Summ_4] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                    if (sqlCommand.ExecuteScalar().ToString() == "0")
                    {
                      sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_4]=" + this.NewPayment.Sum + ",[DateOplati_4]='" + this.NewPayment.DateOplati + "',[Kassir_4]=" + this.NewPayment.Kassir + ",[DateTransaction_4]='" + this.NewPayment.DateTransaction + "',[Check_4]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                      if (sqlCommand.ExecuteNonQuery() <= 0)
                      {
                        this.SendingNetSend(4);
                        this.conn.Close();
                        System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                        this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                        return false;
                      }
                      if (!this.MovToAllIn(this.NewPayment.Order))
                      {
                        this.SendingNetSend(4);
                        this.conn.Close();
                        System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                        this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                        return false;
                      }
                    }
                    else
                    {
                      sqlCommand.CommandText = "SELECT TOP 1 [Summ_5] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                      if (sqlCommand.ExecuteScalar().ToString() == "0")
                      {
                        sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_5]=" + this.NewPayment.Sum + ",[DateOplati_5]='" + this.NewPayment.DateOplati + "',[Kassir_5]=" + this.NewPayment.Kassir + ",[DateTransaction_5]='" + this.NewPayment.DateTransaction + "',[Check_5]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                        if (sqlCommand.ExecuteNonQuery() <= 0)
                        {
                          this.SendingNetSend(4);
                          this.conn.Close();
                          System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                          this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                          return false;
                        }
                        if (!this.MovToAllIn(this.NewPayment.Order))
                        {
                          this.SendingNetSend(4);
                          this.conn.Close();
                          System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                          this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                          return false;
                        }
                      }
                      else
                      {
                        sqlCommand.CommandText = "SELECT TOP 1 [Summ_6] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                        if (sqlCommand.ExecuteScalar().ToString() == "0")
                        {
                          sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_6]=" + this.NewPayment.Sum + ",[DateOplati_6]='" + this.NewPayment.DateOplati + "',[Kassir_6]=" + this.NewPayment.Kassir + ",[DateTransaction_6]='" + this.NewPayment.DateTransaction + "',[Check_6]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                          if (sqlCommand.ExecuteNonQuery() <= 0)
                          {
                            this.SendingNetSend(4);
                            this.conn.Close();
                            System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                            this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                            return false;
                          }
                          if (!this.MovToAllIn(this.NewPayment.Order))
                          {
                            this.SendingNetSend(4);
                            this.conn.Close();
                            System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                            this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                            return false;
                          }
                        }
                        else
                        {
                          sqlCommand.CommandText = "SELECT TOP 1 [Summ_7] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                          if (sqlCommand.ExecuteScalar().ToString() == "0")
                          {
                            sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_7]=" + this.NewPayment.Sum + ",[DateOplati_7]='" + this.NewPayment.DateOplati + "',[Kassir_7]=" + this.NewPayment.Kassir + ",[DateTransaction_7]='" + this.NewPayment.DateTransaction + "',[Check_7]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                            if (sqlCommand.ExecuteNonQuery() <= 0)
                            {
                              this.SendingNetSend(4);
                              this.conn.Close();
                              System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                              this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                              return false;
                            }
                            if (!this.MovToAllIn(this.NewPayment.Order))
                            {
                              this.SendingNetSend(4);
                              this.conn.Close();
                              System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                              this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                              return false;
                            }
                          }
                          else
                          {
                            sqlCommand.CommandText = "SELECT TOP 1 [Summ_8] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                            if (sqlCommand.ExecuteScalar().ToString() == "0")
                            {
                              sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_8]=" + this.NewPayment.Sum + ",[DateOplati_8]='" + this.NewPayment.DateOplati + "',[Kassir_8]=" + this.NewPayment.Kassir + ",[DateTransaction_8]='" + this.NewPayment.DateTransaction + "',[Check_8]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                              if (sqlCommand.ExecuteNonQuery() <= 0)
                              {
                                this.SendingNetSend(4);
                                this.conn.Close();
                                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                                return false;
                              }
                              if (!this.MovToAllIn(this.NewPayment.Order))
                              {
                                this.SendingNetSend(4);
                                this.conn.Close();
                                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                                return false;
                              }
                            }
                            else
                            {
                              sqlCommand.CommandText = "SELECT TOP 1 [Summ_9] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                              if (sqlCommand.ExecuteScalar().ToString() == "0")
                              {
                                sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_9]=" + this.NewPayment.Sum + ",[DateOplati_9]='" + this.NewPayment.DateOplati + "',[Kassir_9]=" + this.NewPayment.Kassir + ",[DateTransaction_9]='" + this.NewPayment.DateTransaction + "',[Check_9]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                                if (sqlCommand.ExecuteNonQuery() <= 0)
                                {
                                  this.SendingNetSend(4);
                                  this.conn.Close();
                                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                  this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                                  return false;
                                }
                                if (!this.MovToAllIn(this.NewPayment.Order))
                                {
                                  this.SendingNetSend(4);
                                  this.conn.Close();
                                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                  this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                                  return false;
                                }
                              }
                              else
                              {
                                sqlCommand.CommandText = "SELECT TOP 1 [Summ_10] FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE [ZakazId] =" + this.NewPayment.Order;
                                if (!(sqlCommand.ExecuteScalar().ToString() == "0"))
                                {
                                  this.SendingNetSend(4);
                                  this.conn.Close();
                                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                  this.WriteToLog("Превышено максимальное количество транзакций для заказа " + this.NewPayment.Order + "(" + filename + ")! Перенесен в BAD!");
                                  return false;
                                }
                                sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_10]=" + this.NewPayment.Sum + ",[DateOplati_10]='" + this.NewPayment.DateOplati + "',[Kassir_10]=" + this.NewPayment.Kassir + ",[DateTransaction_10]='" + this.NewPayment.DateTransaction + "',[Check_10]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.NewPayment.Order;
                                if (sqlCommand.ExecuteNonQuery() <= 0)
                                {
                                  this.SendingNetSend(4);
                                  this.conn.Close();
                                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                  this.WriteToLog("Не удалось добавить информацию об оплате!(Файл " + filename + ")! Перенесен в BAD!");
                                  return false;
                                }
                                if (!this.MovToAllIn(this.NewPayment.Order))
                                {
                                  this.SendingNetSend(4);
                                  this.conn.Close();
                                  System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                  this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
                                  return false;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            this.conn.Close();
            flag = true;
          }
          else
          {
            this.SendingNetSend(4);
            this.conn.Close();
            System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
            this.WriteToLog("В файле " + filename + " обнаружено превышение общей суммы заказа! Перенесен в BAD!");
            flag = false;
          }
        }
        else
        {
          this.NewPayment.Kassir = "1001338";
          this.SendingNetSend(4);
          this.conn.Close();
          System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
          this.WriteToLog("В файле " + filename + " указаны неккоректные данные! Перенесен в BAD!");
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.conn.Close();
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag = false;
      }
      return flag;
    }

    private bool ModDataInTable(string path, string filename)
    {
      bool flag;
      try
      {
        this.conn.Open();
        string sourceFileName = path + "\\" + filename;
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        if (this.GetSpr(ref this.NewPayment) && this.GetSpr(ref this.ModPayment))
        {
          if (this.ChekOrdSumOnMod(this.NewPayment.Order, this.NewPayment.Sum, this.ModPayment.Sum))
          {
            sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_10]=" + this.NewPayment.Sum + ", [DateOplati_10]='" + this.NewPayment.DateOplati + "', [Kassir_10]=" + this.NewPayment.Kassir + ", [DateTransaction_10]='" + this.NewPayment.DateTransaction + "', [Check_10]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_10]=" + this.ModPayment.Sum + " AND [DateOplati_10]='" + this.ModPayment.DateOplati + "' AND [Kassir_10]=" + this.ModPayment.Kassir + " AND [Check_10]=" + this.ModPayment.Check;
            if (sqlCommand.ExecuteNonQuery() == 0)
            {
              sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_9]=" + this.NewPayment.Sum + ", [DateOplati_9]='" + this.NewPayment.DateOplati + "', [Kassir_9]=" + this.NewPayment.Kassir + ", [DateTransaction_9]='" + this.NewPayment.DateTransaction + "', [Check_9]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_9]=" + this.ModPayment.Sum + " AND [DateOplati_9]='" + this.ModPayment.DateOplati + "' AND [Kassir_9]=" + this.ModPayment.Kassir + " AND [Check_9]=" + this.ModPayment.Check;
              if (sqlCommand.ExecuteNonQuery() == 0)
              {
                sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_8]=" + this.NewPayment.Sum + ", [DateOplati_8]='" + this.NewPayment.DateOplati + "', [Kassir_8]=" + this.NewPayment.Kassir + ", [DateTransaction_8]='" + this.NewPayment.DateTransaction + "', [Check_8]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_8]=" + this.ModPayment.Sum + " AND [DateOplati_8]='" + this.ModPayment.DateOplati + "' AND [Kassir_8]=" + this.ModPayment.Kassir + " AND [Check_8]=" + this.ModPayment.Check;
                if (sqlCommand.ExecuteNonQuery() == 0)
                {
                  sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_7]=" + this.NewPayment.Sum + ", [DateOplati_7]='" + this.NewPayment.DateOplati + "', [Kassir_7]=" + this.NewPayment.Kassir + ", [DateTransaction_7]='" + this.NewPayment.DateTransaction + "', [Check_7]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_7]=" + this.ModPayment.Sum + " AND [DateOplati_7]='" + this.ModPayment.DateOplati + "' AND [Kassir_7]=" + this.ModPayment.Kassir + " AND [Check_7]=" + this.ModPayment.Check;
                  if (sqlCommand.ExecuteNonQuery() == 0)
                  {
                    sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_6]=" + this.NewPayment.Sum + ", [DateOplati_6]='" + this.NewPayment.DateOplati + "', [Kassir_6]=" + this.NewPayment.Kassir + ", [DateTransaction_6]='" + this.NewPayment.DateTransaction + "', [Check_6]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_6]=" + this.ModPayment.Sum + " AND [DateOplati_6]='" + this.ModPayment.DateOplati + "' AND [Kassir_6]=" + this.ModPayment.Kassir + " AND [Check_6]=" + this.ModPayment.Check;
                    if (sqlCommand.ExecuteNonQuery() == 0)
                    {
                      sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_5]=" + this.NewPayment.Sum + ", [DateOplati_5]='" + this.NewPayment.DateOplati + "', [Kassir_5]=" + this.NewPayment.Kassir + ", [DateTransaction_5]='" + this.NewPayment.DateTransaction + "', [Check_5]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_5]=" + this.ModPayment.Sum + " AND [DateOplati_5]='" + this.ModPayment.DateOplati + "' AND [Kassir_5]=" + this.ModPayment.Kassir + " AND [Check_5]=" + this.ModPayment.Check;
                      if (sqlCommand.ExecuteNonQuery() == 0)
                      {
                        sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_4]=" + this.NewPayment.Sum + ", [DateOplati_4]='" + this.NewPayment.DateOplati + "', [Kassir_4]=" + this.NewPayment.Kassir + ", [DateTransaction_4]='" + this.NewPayment.DateTransaction + "', [Check_4]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_4]=" + this.ModPayment.Sum + " AND [DateOplati_4]='" + this.ModPayment.DateOplati + "' AND [Kassir_4]=" + this.ModPayment.Kassir + " AND [Check_4]=" + this.ModPayment.Check;
                        if (sqlCommand.ExecuteNonQuery() == 0)
                        {
                          sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_3]=" + this.NewPayment.Sum + ", [DateOplati_3]='" + this.NewPayment.DateOplati + "', [Kassir_3]=" + this.NewPayment.Kassir + ", [DateTransaction_3]='" + this.NewPayment.DateTransaction + "', [Check_3]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_3]=" + this.ModPayment.Sum + " AND [DateOplati_3]='" + this.ModPayment.DateOplati + "' AND [Kassir_3]=" + this.ModPayment.Kassir + " AND [Check_3]=" + this.ModPayment.Check;
                          if (sqlCommand.ExecuteNonQuery() == 0)
                          {
                            sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_2]=" + this.NewPayment.Sum + ", [DateOplati_2]='" + this.NewPayment.DateOplati + "', [Kassir_2]=" + this.NewPayment.Kassir + ", [DateTransaction_2]='" + this.NewPayment.DateTransaction + "', [Check_2]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_2]=" + this.ModPayment.Sum + " AND [DateOplati_2]='" + this.ModPayment.DateOplati + "' AND [Kassir_2]=" + this.ModPayment.Kassir + " AND [Check_2]=" + this.ModPayment.Check;
                            if (sqlCommand.ExecuteNonQuery() == 0)
                            {
                              sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_1]=" + this.NewPayment.Sum + ", [DateOplati_1]='" + this.NewPayment.DateOplati + "', [Kassir_1]=" + this.NewPayment.Kassir + ", [DateTransaction_1]='" + this.NewPayment.DateTransaction + "', [Check_1]=" + this.NewPayment.Check + " WHERE ZakazId=" + this.ModPayment.Order + " AND [Summ_1]=" + this.ModPayment.Sum + " AND [DateOplati_1]='" + this.ModPayment.DateOplati + "' AND [Kassir_1]=" + this.ModPayment.Kassir + " AND [Check_1]=" + this.ModPayment.Check;
                              if (sqlCommand.ExecuteNonQuery() == 0)
                              {
                                this.conn.Close();
                                System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                                this.WriteToLog("В файле " + filename + " указаны неизвестная оплата! Перенесен в BAD!");
                                return false;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            if (!this.MovToAllIn(this.NewPayment.Order))
            {
              this.conn.Close();
              System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
              this.WriteToLog("Не удалось перевести на 4.3!(Файл " + filename + ")! Перенесен в BAD!");
              flag = false;
            }
            else
            {
              this.conn.Close();
              flag = true;
            }
          }
          else
          {
            this.conn.Close();
            System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
            this.WriteToLog("В файле " + filename + " обнаружено превышение общей суммы заказа! Перенесен в BAD!");
            flag = false;
          }
        }
        else
        {
          this.conn.Close();
          System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
          this.WriteToLog("В файле " + filename + " указаны неккоректные данные! Перенесен в BAD!");
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.conn.Close();
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag = false;
      }
      return flag;
    }

    private bool DelDataInTable(string path, string filename)
    {
      bool flag;
      try
      {
        this.conn.Open();
        string sourceFileName = path + "\\" + filename;
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        if (this.GetSpr(ref this.NewPayment))
        {
          sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_10]=0, [DateOplati_10]=NULL, [Kassir_10]=NULL, [DateTransaction_10]=NULL, [Check_10]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_10]=" + this.NewPayment.Sum + " AND [DateOplati_10]='" + this.NewPayment.DateOplati + "' AND [Kassir_10]=" + this.NewPayment.Kassir + " AND [Check_10]=" + this.NewPayment.Check;
          if (sqlCommand.ExecuteNonQuery() == 0)
          {
            sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_9]=0, [DateOplati_9]=NULL, [Kassir_9]=NULL, [DateTransaction_9]=NULL, [Check_9]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_9]=" + this.NewPayment.Sum + " AND [DateOplati_9]='" + this.NewPayment.DateOplati + "' AND [Kassir_9]=" + this.NewPayment.Kassir + " AND [Check_9]=" + this.NewPayment.Check;
            if (sqlCommand.ExecuteNonQuery() == 0)
            {
              sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_8]=0, [DateOplati_8]=NULL, [Kassir_8]=NULL, [DateTransaction_8]=NULL, [Check_8]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_8]=" + this.NewPayment.Sum + " AND [DateOplati_8]='" + this.NewPayment.DateOplati + "' AND [Kassir_8]=" + this.NewPayment.Kassir + " AND [Check_8]=" + this.NewPayment.Check;
              if (sqlCommand.ExecuteNonQuery() == 0)
              {
                sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_7]=0, [DateOplati_7]=NULL, [Kassir_7]=NULL, [DateTransaction_7]=NULL, [Check_7]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_7]=" + this.NewPayment.Sum + " AND [DateOplati_7]='" + this.NewPayment.DateOplati + "' AND [Kassir_7]=" + this.NewPayment.Kassir + " AND [Check_7]=" + this.NewPayment.Check;
                if (sqlCommand.ExecuteNonQuery() == 0)
                {
                  sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_6]=0, [DateOplati_6]=NULL, [Kassir_6]=NULL, [DateTransaction_6]=NULL, [Check_6]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_6]=" + this.NewPayment.Sum + " AND [DateOplati_6]='" + this.NewPayment.DateOplati + "' AND [Kassir_6]=" + this.NewPayment.Kassir + " AND [Check_6]=" + this.NewPayment.Check;
                  if (sqlCommand.ExecuteNonQuery() == 0)
                  {
                    sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_5]=0, [DateOplati_5]=NULL, [Kassir_5]=NULL, [DateTransaction_5]=NULL, [Check_5]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_5]=" + this.NewPayment.Sum + " AND [DateOplati_5]='" + this.NewPayment.DateOplati + "' AND [Kassir_5]=" + this.NewPayment.Kassir + " AND [Check_5]=" + this.NewPayment.Check;
                    if (sqlCommand.ExecuteNonQuery() == 0)
                    {
                      sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_4]=0, [DateOplati_4]=NULL, [Kassir_4]=NULL, [DateTransaction_4]=NULL, [Check_4]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_4]=" + this.NewPayment.Sum + " AND [DateOplati_4]='" + this.NewPayment.DateOplati + "' AND [Kassir_4]=" + this.NewPayment.Kassir + " AND [Check_4]=" + this.NewPayment.Check;
                      if (sqlCommand.ExecuteNonQuery() == 0)
                      {
                        sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_3]=0, [DateOplati_3]=NULL, [Kassir_3]=NULL, [DateTransaction_3]=NULL, [Check_3]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_3]=" + this.NewPayment.Sum + " AND [DateOplati_3]='" + this.NewPayment.DateOplati + "' AND [Kassir_3]=" + this.NewPayment.Kassir + " AND [Check_3]=" + this.NewPayment.Check;
                        if (sqlCommand.ExecuteNonQuery() == 0)
                        {
                          sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[PostupleniaFinansov] SET [Summ_2]=0, [DateOplati_2]=NULL, [Kassir_2]=NULL, [DateTransaction_2]=NULL, [Check_2]=NULL WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_2]=" + this.NewPayment.Sum + " AND [DateOplati_2]='" + this.NewPayment.DateOplati + "' AND [Kassir_2]=" + this.NewPayment.Kassir + " AND [Check_2]=" + this.NewPayment.Check;
                          if (sqlCommand.ExecuteNonQuery() == 0)
                          {
                            sqlCommand.CommandText = "DELETE FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_1]=" + this.NewPayment.Sum + " AND [DateOplati_1]='" + this.NewPayment.DateOplati + "' AND [Kassir_1]=" + this.NewPayment.Kassir + " AND [Check_1]=" + this.NewPayment.Check;
                            if (sqlCommand.ExecuteNonQuery() == 0)
                            {
                              this.conn.Close();
                              System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
                              this.WriteToLog("В файле " + filename + " указаны неизвестная оплата! Перенесен в BAD!");
                              return false;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          sqlCommand.CommandText = "DELETE FROM [intexOrder].[dbo].[PostupleniaFinansov] WHERE ZakazId=" + this.NewPayment.Order + " AND [Summ_1]=0  AND [Summ_2]=0  AND [Summ_3]=0 AND [Summ_4]=0 AND [Summ_5]=0 AND [Summ_6]=0 AND [Summ_7]=0 AND [Summ_8]=0 AND [Summ_9]=0 AND [Summ_10]=0";
          sqlCommand.ExecuteNonQuery();
          this.conn.Close();
          flag = true;
        }
        else
        {
          this.conn.Close();
          System.IO.File.Move(sourceFileName, path + "\\bad\\" + filename);
          this.WriteToLog("В файле " + filename + " указаны неккоректные данные! Перенесен в BAD!");
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.conn.Close();
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag = false;
      }
      return flag;
    }

    private bool FirstPayment(string OrderID)
    {
      bool flag1 = false;
      bool flag2;
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT ISNULL([PredvaritZakaz],1) FROM [Заказы] where [key]=" + OrderID;
        if (Convert.ToInt32(sqlCommand.ExecuteScalar()) != 0)
        {
          sqlCommand.CommandText = "[dbo].[GetManagerID]";
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.AddWithValue("@OrderID", (object) OrderID);
          string str = sqlCommand.ExecuteScalar().ToString();
          sqlCommand.Parameters.Clear();
          sqlCommand.CommandType = CommandType.Text;
          if (str != "")
          {
            sqlCommand.CommandText = " SELECT [Microstad] FROM [Заказы] WHERE [Key]=" + OrderID;
            string LastStadia = sqlCommand.ExecuteScalar().ToString();
            DateTime dateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            sqlCommand.CommandText = "UPDATE [intexOrder].[dbo].[Заказы] SET [MicroStad] = 1300, [PredvaritZakaz]=0, [Prinimator]=" + str + ", [Data_priema]='" + string.Format("{0:yyyy-MM-dd}", (object) dateTime) + "' WHERE [MicroStad]<1300 AND [Key]=" + OrderID;
            if (sqlCommand.ExecuteNonQuery() > 0)
            {
              this.InsertIntoHistory(OrderID, "1300", this.NewPayment.Kassir, LastStadia);
              flag1 = true;
              flag2 = flag1;
              this.WriteToLog("назначение менеджера " + OrderID);
              this.setOrderManagerInNik(OrderID);
              this.WriteToLog("проверка рекомендаций " + OrderID);
              this.checkOrderReferenceInNik(OrderID);
            }
            else
            {
              this.WriteToLog("Не удалось переместить на стадию 2.0!");
              flag2 = flag1;
            }
          }
          else
          {
            this.WriteToLog("Не удалось определить менеджера!");
            flag2 = flag1;
          }
        }
        else
          flag2 = true;
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag2 = flag1;
      }
      return flag2;
    }

    private bool MovToAllIn(string OrderID)
    {
      bool flag;
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT ISNULL((StoimonstOnRegPosrKoef-(Summ_1+Summ_2+Summ_3+Summ_4+Summ_5+Summ_6+Summ_7+Summ_8+Summ_9+Summ_10)),-1) FROM dbo.Заказы INNER JOIN dbo.PostupleniaFinansov ON [Key]=ZakazId WHERE ([Microstad]=2300 OR [Microstad]=2400) AND [Key]=" + OrderID;
        object obj = sqlCommand.ExecuteScalar();
        if (obj != null && obj != DBNull.Value && Convert.ToDouble(obj) == 0.0)
        {
          sqlCommand.CommandText = " SELECT [Microstad] FROM [Заказы] WHERE [Key]=" + OrderID;
          string LastStadia = sqlCommand.ExecuteScalar().ToString();
          sqlCommand.CommandText = " UPDATE [Заказы] SET [Microstad]=2500 WHERE [Key]=" + OrderID;
          if (sqlCommand.ExecuteNonQuery() > 0)
          {
            this.InsertIntoHistory(OrderID, "2500", this.NewPayment.Kassir, LastStadia);
            this.SendingNetSend(3);
            flag = true;
          }
          else
          {
            this.WriteToLog("не удалось перевести на стадию 4.3!");
            flag = false;
          }
        }
        else
        {
          this.SendingNetSend(2);
          flag = true;
        }
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag = false;
      }
      return flag;
    }

    private bool InsertIntoHistory(string OrderID, string Stadia, string kassir, string LastStadia)
    {
      SqlConnection sqlConnection = new SqlConnection();
      sqlConnection.ConnectionString = "Data Source=" + this.host + ";Initial Catalog=" + this.dbase + ";User Id=" + this.duser + ";Password=" + this.dpass + ";";
      SqlCommand sqlCommand = new SqlCommand();
      sqlCommand.Connection = sqlConnection;
      DateTime dateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
      sqlCommand.CommandText = "INSERT INTO [intexOrder].[dbo].[OrderHistoryOfTranserStadia] ([OrderID],[SotrudnikID],[LastStadiaID],[NewStadidID],[TransferData],[Motiv]) VALUES (" + OrderID + ", '" + kassir + "' ," + LastStadia + " ," + Stadia + " ,'" + this.NewPayment.DateTransaction + "' ,'Автоматический перевод из 1С')";
      bool flag;
      try
      {
        sqlConnection.Open();
        if (sqlCommand.ExecuteNonQuery() > 0)
        {
          sqlConnection.Close();
          flag = true;
        }
        else
        {
          sqlConnection.Close();
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        sqlConnection.Close();
        flag = false;
      }
      return flag;
    }

    private bool ChekOrdSum(string IDord, string PaySum)
    {
      double num1 = 0.0;
      bool flag;
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT TOP 1 [StoimonstOnRegPosrKoef] FROM [intexOrder].[dbo].[Заказы] where [Key]=" + IDord;
        object obj1 = sqlCommand.ExecuteScalar();
        if (obj1 != null && obj1 != DBNull.Value)
        {
          double num2 = (double) obj1;
          sqlCommand.CommandText = "SELECT Summ_1 + Summ_2 + Summ_3 + Summ_4 + Summ_5 + Summ_6 + Summ_7 + Summ_8 + Summ_9 + Summ_10 AS Sum FROM [intexOrder].[dbo].[PostupleniaFinansov] where [ZakazId]=" + IDord;
          object obj2 = sqlCommand.ExecuteScalar();
          if (obj2 != null && obj2 != DBNull.Value)
            num1 = (double) obj2;
          double num3 = num1 + Convert.ToDouble(PaySum);
          if (num2 >= num3)
          {
            flag = true;
          }
          else
          {
            this.WriteToLog("Превышение общей суммы заказа!");
            flag = false;
          }
        }
        else
        {
          this.WriteToLog("Не удалось найти такой заказ!");
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag = false;
      }
      return flag;
    }

    private bool ChekOrdSumOnMod(string IDord, string PaySum, string ModSum)
    {
      double num1 = 0.0;
      bool flag;
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT TOP 1 [StoimonstOnRegPosrKoef] FROM [intexOrder].[dbo].[Заказы] where [Key]=" + IDord;
        object obj1 = sqlCommand.ExecuteScalar();
        if (obj1 != null && obj1 != DBNull.Value)
        {
          double num2 = (double) obj1;
          sqlCommand.CommandText = "SELECT Summ_1 + Summ_2 + Summ_3 + Summ_4 + Summ_5 + Summ_6 + Summ_7 + Summ_8 + Summ_9 + Summ_10 AS Sum FROM [intexOrder].[dbo].[PostupleniaFinansov] where [ZakazId]=" + IDord;
          object obj2 = sqlCommand.ExecuteScalar();
          if (obj2 != null && obj2 != DBNull.Value)
            num1 = (double) obj2;
          double num3 = num1 + Convert.ToDouble(PaySum) - Convert.ToDouble(ModSum);
          if (num2 >= num3)
          {
            flag = true;
          }
          else
          {
            this.WriteToLog("Превышение общей суммы заказа!");
            flag = false;
          }
        }
        else
        {
          this.WriteToLog("Не удалось найти такой заказ!");
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
        flag = false;
      }
      return flag;
    }

    private string GetCheckCode(string CheckName)
    {
      string str = "";
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT TOP 1 [MoneyStorageID] FROM [intexOrder].[dbo].[MoneyStorages] where [MoneyStorageName]='" + CheckName + "'";
        object obj = sqlCommand.ExecuteScalar();
        if (obj != null && obj != DBNull.Value)
          str = obj.ToString();
        else
          this.WriteToLog("Не удалось найти такой счет!");
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
      }
      return str;
    }

    private string GetZakID(string PosrKode)
    {
      string str = "";
      this.KodPosrednika = PosrKode;
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT TOP 1 [Key] FROM [intexOrder].[dbo].[Заказы] where [Kod_posrednika]='" + PosrKode + "'";
        object obj = sqlCommand.ExecuteScalar();
        if (obj != null && obj != DBNull.Value)
          str = obj.ToString();
        else
          this.WriteToLog("Не удалось найти такой заказ!");
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
      }
      return str;
    }

    private string GetKassirID(string KassirName)
    {
      string str = "";
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT TOP 1 [КодСотрудника] FROM [intexOrder].[dbo].[Сотрудники] where [name]='" + KassirName + "'";
        object obj = sqlCommand.ExecuteScalar();
        if (obj != null && obj != DBNull.Value)
          str = obj.ToString();
        else
          this.WriteToLog("Не удалось найти такого сотрудника!");
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
      }
      return str;
    }

    private bool GetSpr(ref Import1C.Payment Pay)
    {
      string checkCode = this.GetCheckCode(Pay.Check);
      string zakId = this.GetZakID(Pay.Order);
      string kassirId = this.GetKassirID(Pay.Kassir);
      if (!(zakId != "") || !(kassirId != "") || !(checkCode != ""))
        return false;
      Pay.Kassir = kassirId;
      Pay.Order = zakId;
      Pay.Check = checkCode;
      Pay.DateTransaction = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
      return true;
    }

    private void WriteToLog(string message)
    {
      this.file.WriteLine(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + " - " + message);
      this.file.Flush();
    }

    private void NetSend(string message, string to)
    {
      if (to == null || !(to != ""))
        return;
      Process process = new Process();
      process.StartInfo.FileName = "cmd.exe";
      process.StartInfo.Arguments = "/C NET SEND " + to + " " + message;
      process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
      process.StartInfo.CreateNoWindow = true;
      process.Start();
      process.WaitForExit();
    }

    private string GetSendName(string idSotr)
    {
      string str = "";
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        sqlCommand.CommandText = "SELECT TOP 1 [DomainName] FROM [intexOrder].[dbo].[DomainNames] where [idSotr]=" + idSotr;
        object obj = sqlCommand.ExecuteScalar();
        if (obj != null)
        {
          if (obj != DBNull.Value)
            str = obj.ToString();
        }
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
      }
      return str;
    }

    private void SendingNetSend(int action)
    {
      string to1 = "";
      string to2 = "";
      string to3 = "";
      string to4 = "";
      string str = "";
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = this.conn;
        switch (action)
        {
          case 1:
            sqlCommand.CommandText = "SELECT TOP 1 DomainName FROM Заказы INNER JOIN DomainNames ON idSotr=AdminPrinimatorID Where [key]=" + this.NewPayment.Order;
            object obj1 = sqlCommand.ExecuteScalar();
            if (obj1 != null && obj1 != DBNull.Value)
              to1 = obj1.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 DomainName FROM Заказы INNER JOIN DomainNames ON idSotr=Prinimator Where [key]=" + this.NewPayment.Order;
            object obj2 = sqlCommand.ExecuteScalar();
            if (obj2 != null && obj2 != DBNull.Value)
              to2 = obj2.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 qip FROM Заказы INNER JOIN DomainNames ON idSotr=AdminPrinimatorID Where [key]=" + this.NewPayment.Order;
            object obj3 = sqlCommand.ExecuteScalar();
            if (obj3 != null && obj3 != DBNull.Value)
              to3 = obj3.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 qip FROM Заказы INNER JOIN DomainNames ON idSotr=Prinimator Where [key]=" + this.NewPayment.Order;
            object obj4 = sqlCommand.ExecuteScalar();
            if (obj4 != null && obj4 != DBNull.Value)
              to4 = obj4.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 Kod_posrednika FROM Заказы Where [key]=" + this.NewPayment.Order;
            object obj5 = sqlCommand.ExecuteScalar();
            if (obj5 != null && obj5 != DBNull.Value)
              str = obj5.ToString();
            if (to1 != "" && str != "")
              this.NetSend("Поступил аванс по заказу " + str + "!", to1);
            if (to2 != "" && str != "")
              this.NetSend("Поступил аванс по заказу " + str + "!", to2);
            if (to3 != "" && str != "")
              this.JIDSend("Поступил аванс по заказу " + str + "!", to3);
            if (to4 != "" && str != "")
              this.JIDSend("Поступил аванс по заказу " + str + "!", to4);
            this.JIDSend("Поступил аванс по заказу " + str + "!", "shanwork@qip.ru");
            break;
          case 2:
            sqlCommand.CommandText = "SELECT TOP 1 DomainName FROM Заказы INNER JOIN DomainNames ON idSotr=Prinimator Where [key]=" + this.NewPayment.Order;
            object obj6 = sqlCommand.ExecuteScalar();
            if (obj6 != null && obj6 != DBNull.Value)
              to2 = obj6.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 qip FROM Заказы INNER JOIN DomainNames ON idSotr=Prinimator Where [key]=" + this.NewPayment.Order;
            object obj7 = sqlCommand.ExecuteScalar();
            if (obj7 != null && obj7 != DBNull.Value)
              to4 = obj7.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 Kod_posrednika FROM Заказы Where [key]=" + this.NewPayment.Order;
            object obj8 = sqlCommand.ExecuteScalar();
            if (obj8 != null && obj8 != DBNull.Value)
              str = obj8.ToString();
            if (to2 != "" && str != "")
              this.NetSend("Поступила оплата по заказу " + str + "!", to2);
            if (to4 != "" && str != "")
              this.JIDSend("Поступила оплата по заказу " + str + "!", to4);
            this.JIDSend("Поступил оплата по заказу " + str + "!", "shanwork@qip.ru");
            break;
          case 3:
            sqlCommand.CommandText = "SELECT TOP 1 DomainName FROM Заказы INNER JOIN DomainNames ON idSotr=Prinimator Where [key]=" + this.NewPayment.Order;
            object obj9 = sqlCommand.ExecuteScalar();
            if (obj9 != null && obj9 != DBNull.Value)
              to2 = obj9.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 qip FROM Заказы INNER JOIN DomainNames ON idSotr=Prinimator Where [key]=" + this.NewPayment.Order;
            object obj10 = sqlCommand.ExecuteScalar();
            if (obj10 != null && obj10 != DBNull.Value)
              to4 = obj10.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 Kod_posrednika FROM Заказы Where [key]=" + this.NewPayment.Order;
            object obj11 = sqlCommand.ExecuteScalar();
            if (obj11 != null && obj11 != DBNull.Value)
              str = obj11.ToString();
            if (to2 != "" && str != "")
              this.NetSend("Поступило погашение по заказу " + str + "!", to2);
            if (to4 != "" && str != "")
              this.JIDSend("Поступило погашение по заказу " + str + "!", to4);
            this.JIDSend("Поступил погашеие по заказу " + str + "!", "shanwork@qip.ru");
            break;
          case 4:
            sqlCommand.CommandText = "SELECT TOP 1 DomainName FROM DomainNames Where [idSotr]=" + this.NewPayment.Kassir;
            object obj12 = sqlCommand.ExecuteScalar();
            if (obj12 != null && obj12 != DBNull.Value)
              to2 = obj12.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 qip FROM DomainNames Where [idSotr]=" + this.NewPayment.Kassir;
            object obj13 = sqlCommand.ExecuteScalar();
            if (obj13 != null && obj13 != DBNull.Value)
              to4 = obj13.ToString();
            sqlCommand.CommandText = "SELECT TOP 1 Kod_posrednika FROM Заказы Where [key]=" + this.NewPayment.Order;
            object obj14 = sqlCommand.ExecuteScalar();
            if (obj14 != null && obj14 != DBNull.Value)
              str = obj14.ToString();
            if (to2 != "" && str != "")
              this.NetSend("Ошибка при обработке платежа по заказу " + str + "!", to2);
            if (to4 != "" && str != "")
              this.JIDSend("Ошибка при обработке платежа по заказу " + str + "!", to4);
            this.JIDSend("Ошибка при обработке платежа по заказу " + str + "!", "shanwork@qip.ru");
            break;
        }
      }
      catch (Exception ex)
      {
        this.WriteToLog("Ошибка при работе с БД:  " + ex.Message);
      }
    }

    private void connected()
    {
      this.connect = 1;
    }

    private void disconnected()
    {
      this.xmpp.Open(new Jid(this.JID_SENDER).User, this.PASSWORD);
      this.connect = 0;
    }

    private void JIDSend(string message, string to)
    {
      if (this.connect != 1)
        return;
      ((XmppConnection) this.xmpp).Send((Element) new Message(new Jid(to), "1", message));
    }

    private string GetURL(string email, string orderid)
    {
      string str = "http://cl.diplomtime.ru/";
      if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(orderid) && (email != "" && orderid != ""))
        str = "http://psw.diplomtime.ru/?val=" + BitConverter.ToString(((HashAlgorithm) CryptoConfig.CreateFromName("MD5")).ComputeHash(new UnicodeEncoding().GetBytes(orderid + email))).Replace("-", "");
      return str;
    }

    private void setOrderManagerInNik(string orderId)
    {
      WebRequest.Create("http://kolia.diplomtime.ru/index.php/orders/by-id/" + orderId + "/assign-manager").GetResponse().Close();
    }

    private void checkOrderReferenceInNik(string orderId)
    {
      WebRequest.Create("http://kolia.diplomtime.ru/index.php/orders/by-id/" + orderId + "/check-reference").GetResponse().Close();
    }

    private struct Payment
    {
      public string Order;
      public string Sum;
      public string DateOplati;
      public string Kassir;
      public string DateTransaction;
      public string Check;

      public void reset()
      {
        this.Order = "";
        this.Sum = "";
        this.DateOplati = "";
        this.Kassir = "";
        this.DateTransaction = "";
        this.Check = "";
      }
    }
  }
}
